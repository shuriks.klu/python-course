import math


def circle_circumference(square, fside, sside=6, tside=3):
    print(math.pi*(fside*sside*tside/(2*square)))


def circle_area(radius):
    print(math.pi*radius**2)


circle_circumference(fside=26, sside=2, tside=5, square=7)
circle_circumference(34, 4)
circle_area(2)
circle_area((lambda x, y: x/y)(9, 3))